# Sử dụng lệnh mount  
1. Gắn thủ công  
Giả sử bạn có ổ đĩa C: định dạng ntfs cài Windows có tên là /dev/sda1. Bây giờ bạn hãy mount vào hệ thống như sau:  
$ `mkdir /mnt/WindowsXP` # Tạo thư mục để gắn sda1 vào  
$ `mount -v -t ntfs /dev/sda1 /mnt/WindowsXP`​  
Từ giờ bạn truy cập vào ổ "C:" bằng đường dẫn /mnt/WindowsXP.  
2. Tháo thiết bị ra khỏi vị trí được gắn trước đó  
$ sudo umount Tên_thư_mục_gắn | Tên_phân_vùng​  
Ví dụ:  
$ `sudo umount /mnt/WindowsXP` # hay có thể dùng:  
$ `sudo umount /dev/sda1`​  
3. Các kiểu mount khác  
Gắn các ổ đĩa mạng bao gồm một số giao thức như nfs, ftp, ssh, ftps v.v.. Việc gắn này có kiểu cần thêm phần mềm cũng có kiểu thì không. Các bạn cần tìm hiểu thêm vì trong một bài viết ngắn thì tôi không thể trình bày tất cả được. Xin đưa ra một ví dụ sau:  
$ `sudo mount máy_chủ_nfsd:/home/demo /mnt/demo​`  
Nếu sử dụng fstab:  
`máy_chủ_nfsd:/home/demo /mnt nfs rw 0 0​`
