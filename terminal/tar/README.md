# linux_tutorial  

Cú pháp cơ bản của tar  
tar option(s)  archive_name file_name(s)  
1. Tạo và giải nén file .tar  
a) Tạo (**C**reate) file *.tar  
$ `tar cvf archive_name.tar dirname/`  
b) Bung (E**x**tract) file *.tar  
Để bung file .tar ta thay c bằng x nhé  
$ `tar xvf archive_name.tar`  
Trong đó :  
**C** : Tạo ra 1 file archive  
**X** : Bung (Extract) file  
**V** : HIện thị danh sách các file  trong  tiến trình  
**F** : tên file archive  
2. Tạo và giải nén file .tar.gz  
a) Tạo (Create) file *.tar.gz  
Để tạo 1 file archive nén(gzip) chúng ta thêm vào đối số z  
$ `tar –cvfz  archive_name.tar dirname/`  
b) Giải nén (Extract) file *.tar.gz  
$ `tar xvfz archive_name.tar.gz`  
Trong Đó :  
**Z** : tạo file archive bằng chương trình Gzip  
Các bạn lưu ý file archive .tar.gz và tgz là một.  
3. Tạo và giải nén file .tar.bz2  
a) Tạo (Create) file *.tar.bz2  
Để tạo 1 file archive nén(bzipped ) chúng ta thêm vào đối số j  
$ `tar -cvfj archive_name.tar dirname/`  
b) Giải nén (Extract) file *.tar.bz2  
$ `tar xvfj archive_name.tar.gz`  
Trong Đó :  
**J** : tạo file archive bằng chương trình bzip2  
Các bạn lưu ý  file  archive .tbz và tb2 là một  
** gzip và gzip2: bzip2 mất nhiều thời gian để nén và giải nén hơn so với gzip.Nhưng file tạo ra bằng gzip2 nhẹ hơn so với gzip
