# vim  
  
1. đường dẫn của file môi trường **vimrc** `/usr/share/vim/`  
2. muốn tìm một file gõ lệnh `sudo updatedb && locate tên_file`  
ví dụ: tìm file vimrc `sudo updatedb && locate vimrc`  
3. set tab size là 4 cho vim  
`set tabstop=4`  
`set shiftwidth=4`  
`set expandtab`  
4. hiện số dòng  
`set number`
